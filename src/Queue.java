
/**
 * 
 * A class that implements a queue.  It is your job to complete this class.  Your queue
 * will use a linked list constructed by QueueElements.  However, your queue must be general and allow
 * setting of any type of Object.  Also you cannot use ArrayLists or arrays (you will get zero).  
 * @author you
 *
 */


import java.util.NoSuchElementException;

public class Queue<T> {

	//TODO:  You need some data to store the queue.  Put the attributes here.
	
	private class Node {
	    T value;
	    Node next;
	  }
	
	private int count;
	private Node front;
	private Node rear;

	
	/**
	 * Constructs an empty Queue.
	 */
	public Queue () {
	    //TODO: Write the Queue constructor
		front = rear = null;
	    count = 0;
	}
	
	/**
	 * Returns true if the queue is empty
	 */
	public boolean isEmpty () {
	    //TODO:  Needs to return true when empty and false otherwise
		return front == null;
	}
	
	
	/**
	 * Returns the element at the head of the queue
	 */
	public T peek () throws NoSuchElementException {
		if(isEmpty()) {
			throw new NoSuchElementException("Queue is empty");
		}
		return front.value;
	}
	
	/**
	 * Removes the front element of the queue
	 */
	public void dequeue () throws NoSuchElementException {
	    //Dequeue code is neede here
		if(isEmpty()) {
			throw new NoSuchElementException("Queue is empty");
		}
		front = front.next;
        count--;
		if (front == null) {
            rear = null;
        }
	}
	
	/**
	 * Puts an element on the back of the queue.
	 */
	public void enqueue (T element) {
	    //Enqueue code is needed here		
		Node temp = new Node();
        temp.value = element;
        temp.next = null;
        if(isEmpty()) {
            front = rear = temp;
        } else {
            rear.next = temp; rear = temp;
        }
        count++;
	}
	
	/**
	 * Method to print the full contents of the queue in order from head to tail.
	 */
	public void print () {
	    //Code to print the code is needed here
		if(isEmpty()) {
			System.out.println("Queue Empty");
			return;
		}
		Node n = front;
		while(n != null) {
			System.out.println(n.value.toString());
			n = n.next;
		}
	}
}
