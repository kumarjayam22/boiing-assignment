import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Square extends ClosedShape {
	
	private int side;

	protected Square(int insertionTime, int x, int y, int vx, int vy, int side, Color colour, boolean isFilled) {
		super(insertionTime, x, y, vx, vy, colour, isFilled);
		// TODO Auto-generated constructor stub
		this.side = side;
	}

	@Override
	public void draw(GraphicsContext g) {
		// TODO Auto-generated method stub
		g.setFill(colour);
		g.setStroke(colour);
		if (isFilled) {
    		g.fillRect(x, y, side, side);
    	} else {
    		g.strokeRect(x, y, side, side);
    	}
		
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return side;
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return side;
	}
	
	/**
     * Method to convert a square to a string.
     */
    public String toString () {
    	String result = "This is a square\n";
    	result += super.toString ();
	result += "Its side is " + this.side + "\n";
    	return result;
    }
}
