import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Rect extends ClosedShape {
	
	private int width, height;
	
	protected Rect(int insertionTime, int x, int y, int vx, int vy, int width, int height, Color colour, boolean isFilled) {
		super(insertionTime, x, y, vx, vy, colour, isFilled);
		// TODO Auto-generated constructor stub
		this.width = width;
		this.height = height;
	}

	/**
     * Method to convert an rect to a string.
     */
    public String toString () {
    	String result = "This is an rectangle\n";
    	result += super.toString ();
	result += "Its width is " + this.width + " and its height is " + this.height + "\n";
    	return result;
    }

	@Override
	public void draw(GraphicsContext g) {
		// TODO Auto-generated method stub
		g.setFill(colour);
		g.setStroke(colour);
		if (isFilled) {
    		g.fillRect(x, y, width, height);
    	} else {
    		g.strokeRect(x, y, width, height);
    	}
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return width;
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return height;
	}
	

}
