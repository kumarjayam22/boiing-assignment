
/**
 * This class reads a shape file.  For the format of this shape file, see the assignment description.
 * Also, please see the shape files ExampleShapes.txt, ExampleShapesStill.txt, and TwoRedCircles.txt
 *
 * @author you
 *
 */


import java.io.*;
import java.util.Scanner;

import javafx.scene.paint.Color;

public class ReadShapeFile {

	// TODO: You will likely need to write four methods here. One method to
	// construct each shape
	// given the Scanner passed as a parameter. I would suggest static
	// methods in this case.

	/**
	 * Reads the data file used by the program and returns the constructed queue
	 *
	 * @param in
	 *            the scanner of the file
	 * @return the queue represented by the data file
	 */
	private static Queue<ClosedShape> readLineByLine(Scanner in) {
		Queue<ClosedShape> shapeQueue = new Queue<ClosedShape>();
		
		while(in.hasNextLine()) {
			
			String[] str = in.nextLine().split(" ");
			if(str[0].toLowerCase().equals("circle")) {
				
				ClosedShape circle = new Circle(Integer.parseInt(str[10]), Integer.parseInt(str[1]), Integer.parseInt(str[2]), 
						Integer.parseInt(str[3]), Integer.parseInt(str[4]), Integer.parseInt(str[6]),
						Color.rgb(Integer.parseInt(str[7]), Integer.parseInt(str[8]), Integer.parseInt(str[7])), 
						Boolean.parseBoolean(str[5]));
				shapeQueue.enqueue(circle);
			}
			
			if(str[0].toLowerCase().equals("oval")) {
				
				ClosedShape oval = new Oval(Integer.parseInt(str[11]), Integer.parseInt(str[1]), Integer.parseInt(str[2]), 
						Integer.parseInt(str[3]), Integer.parseInt(str[4]), Integer.parseInt(str[6]), Integer.parseInt(str[7]),
						Color.rgb(Integer.parseInt(str[8]), Integer.parseInt(str[9]), Integer.parseInt(str[10])), 
						Boolean.parseBoolean(str[5]));
				shapeQueue.enqueue(oval);
			}
			if(str[0].toLowerCase().equals("square")) {
				
				ClosedShape square = new Square(Integer.parseInt(str[10]), Integer.parseInt(str[1]), Integer.parseInt(str[2]), 
						Integer.parseInt(str[3]), Integer.parseInt(str[4]), Integer.parseInt(str[6]),
						Color.rgb(Integer.parseInt(str[7]), Integer.parseInt(str[8]), Integer.parseInt(str[7])), 
						Boolean.parseBoolean(str[5]));
				shapeQueue.enqueue(square);
			}
			
			if(str[0].toLowerCase().equals("rect")) {
				
				ClosedShape rect = new Rect(Integer.parseInt(str[11]), Integer.parseInt(str[1]), Integer.parseInt(str[2]), 
						Integer.parseInt(str[3]), Integer.parseInt(str[4]), Integer.parseInt(str[6]), Integer.parseInt(str[7]),
						Color.rgb(Integer.parseInt(str[8]), Integer.parseInt(str[9]), Integer.parseInt(str[10])), 
						Boolean.parseBoolean(str[5]));
				shapeQueue.enqueue(rect);
			}
		}
		shapeQueue.print();
		return shapeQueue;
	}





	/**
	 * Method to read the file and return a queue of shapes from this file. The
	 * program should handle the file not found exception here and shut down the
	 * program gracefully.
	 *
	 * @param filename
	 *            the name of the file
	 * @return the queue of shapes from the file
	 */
	public static Queue<ClosedShape> readDataFile(String filename) {
	    // HINT: You might want to open a file here.
		File file = new File(filename);
		Scanner in = null;
		try {
			in = new Scanner(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Could not find" + filename);
		}

	    return ReadShapeFile.readLineByLine(in);

	}
}
